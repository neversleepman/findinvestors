import vk_api
from vk_api.requests_pool import RequestResult

VERSION = 5.73
TOKEN = "bd5794507746a9835dc252680d851e915c19653cf18cbed6389faad2222c29a6d85a7231f73a253339678"
ID = 6447501
russia = 1


def get25groupsIDs(session):
    with vk_api.VkRequestsPool(session) as pool:
        groups = pool.method('groups.search', {
            "q": "Инвест",
            "count": 25,
            "country": 1
        })
    return [x['id'] for x in groups.result['items']]


def get1000MembersFromEveryGroup(session, groupsIds):
    groupMembersIds = {}
    with vk_api.VkRequestsPool(session) as pool:
        for group_id in groupsIds:
            groupMembersIds[group_id] = pool.method('groups.getMembers', {
                "group_id": group_id
            })
    return groupMembersIds


def getSetOfUsers(groupMembersIds):
    usersIds = set()
    for k, v in groupMembersIds.items():
        usersIds |= set(v.result['items'])
    return usersIds



session = vk_api.VkApi(token=TOKEN)
groupsIds = get25groupsIDs(session)
groupMembersIds = get1000MembersFromEveryGroup(session, groupsIds)
usersIds = getSetOfUsers(groupMembersIds)
print(usersIds)
