from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup
from telegram.ext import (Updater, CommandHandler, CallbackQueryHandler, MessageHandler,
                          Filters, RegexHandler, ConversationHandler)
import logging
from time import time

tab = ''


def timer(func):
    def f(*args, **kwargs):
        global tab
        tab += '  '
        print('{}Выполняется {.__name__}'.format(tab, func))
        before = time()
        rv = func(*args, **kwargs)
        after = time()
        print('{}Время выполнения'.format(tab), round(after - before, 2))
        tab = tab[:-2]
        if len(tab) == 0:
            print('----'*20)
        return rv
    return f

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


CHOOSING = range(1)


@timer
def start(bot, update):
    update.message.reply_text(
        'Вас приветствует InvestFinderBot!\nВыберите социальную сеть в которой нам следует произвести поиск\nСписок доступных соц.сетей:\nvk.com /vk'
        )
    return CHOOSING


@timer
def vk_parse(bot, update):
    return CHOOSING


@timer
def finish(bot, update):
    return ConversationHandler.END


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    """Start the bot."""
    # Create the EventHandler and pass it your bot's token.
    updater = Updater("502140889:AAHeMUuZAULKMIlLJ7pY7iZE8b4GUr703TY")

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("start", start)],

        states={
            CHOOSING: [CommandHandler('vk', vk_parse)],
        },

        fallbacks=[CommandHandler('finish', finish)]
    )

    dp.add_handler(conv_handler)
    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # End the bot by ctrl+c
    updater.idle()


if __name__ == '__main__':
    main()
